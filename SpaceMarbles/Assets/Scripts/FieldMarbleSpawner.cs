﻿using UnityEngine;

public class FieldMarbleSpawner : MarbleSpawner
{
    [SerializeField]
    private int numberOfMarblesToSpawnPerPlayer = 3;

    protected override void Start()
    {
        base.Start();
        for (int i = 0; i < numberOfMarblesToSpawnPerPlayer * gameManager.numberOfPlayers; i++)
        {
            var randomPositionInsideCircleCollider = (Vector3)Random.insideUnitCircle * gameObject.GetComponent<CircleCollider2D>().radius + transform.position;
            SpawnMarble(randomPositionInsideCircleCollider);
            gameManager.ChangeCurrentPlayer();
        }
    }
}
