﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldScript : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Marble"))
        {
            var marble = other.gameObject.GetComponent<PlanetMarble>();
            if (marble.player != -1)
            {
                FindObjectOfType<GameManager>().IncreaseScore(marble.scoreValue, marble.player);
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Marble"))
        {
            var marble = other.gameObject.GetComponent<PlanetMarble>();
            if (marble.player != -1)
            {
                FindObjectOfType<GameManager>().IncreaseScore(-marble.scoreValue, marble.player);
            }
        }
    }
}
