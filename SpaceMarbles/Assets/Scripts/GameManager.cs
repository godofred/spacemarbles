﻿using Cinemachine;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public List<Sprite> playerMarbleSprites;

    [HideInInspector]
    public int[] Score;
    [HideInInspector]
    public int currentPlayer;
    public int numberOfPlayers = 2;

    public TextMeshProUGUI player1ScoreTextUI;
    public TextMeshProUGUI player1TextUI;
    public TextMeshProUGUI player2ScoreTextUI;
    public TextMeshProUGUI player2TextUI;
    public GameObject endGameTextUI;

    private bool moveLeftRequested;
    private bool moveRightRequested;
    public float RotateSpeed = 5f;
    private Vector2 _centre;
    private CinemachineVirtualCamera vCam;
    [HideInInspector]
    public GameObject lastMarbleSpawned;

    [SerializeField]
    private GameObject playerMarbleSpawner;
    private MarbleSpawner marbleSpawner;

    public float timeToWaitBeforeRestart = 3;

    private bool restartTrigger = false;
    private float resetTime;

    protected Joystick mobileJoystick;
    private float horizontalAxis;

    void Start()
    {
        player1ScoreTextUI.text = "0";
        player2ScoreTextUI.text = "0";
        Score = new int[2];
        currentPlayer = 0;
        Player1Turn();

        marbleSpawner = playerMarbleSpawner.GetComponent<MarbleSpawner>();
        marbleSpawner.SpawnMarble(playerMarbleSpawner.gameObject.transform.position);

        mobileJoystick = FindObjectOfType<Joystick>();
        var fieldScript = FindObjectOfType<FieldScript>();
        
        _centre = fieldScript.gameObject.transform.position;
        vCam = FindObjectOfType<CinemachineVirtualCamera>();
    }

    private void Player1Turn()
    {
        player1ScoreTextUI.fontSize = 28;
        player2ScoreTextUI.alpha = 0.66f;
        player1TextUI.fontSize = 28;
        player2TextUI.alpha = 0.66f;

        player2ScoreTextUI.fontSize = 21;
        player1ScoreTextUI.alpha = 1.0f;
        player2TextUI.fontSize = 21;
        player1TextUI.alpha = 1.0f;
    }

    private void Player2Turn()
    {
        player2ScoreTextUI.fontSize = 28;
        player1ScoreTextUI.alpha = 0.66f;
        player2TextUI.fontSize = 28;
        player1TextUI.alpha = 0.66f;

        player1ScoreTextUI.fontSize = 21;
        player2ScoreTextUI.alpha = 1.0f;
        player1TextUI.fontSize = 21;
        player2TextUI.alpha = 1.0f;
    }

    private void Update()
    {
        horizontalAxis = Input.GetAxis("Horizontal") + (mobileJoystick.Horizontal);
        if (horizontalAxis > 0.12f)
        {
            moveLeftRequested = true;
        }
        else if (horizontalAxis < -0.12f)
        {
            moveRightRequested = true;
        }
        if (restartTrigger && resetTime <= Time.time)
        {
            SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        }
    }

    public void RestartTheGameAfterSomeTime()
    {
        restartTrigger = true;
        resetTime = Time.time + timeToWaitBeforeRestart;
    }

    private void FixedUpdate()
    {
        if (moveLeftRequested && !lastMarbleSpawned.GetComponent<PlanetMarble>().isDraggingMouse && !lastMarbleSpawned.GetComponent<PlanetMarble>().marbleMoving)
        {
            moveLeftRequested = false;
            playerMarbleSpawner.transform.RotateAround(_centre, Vector3.forward, RotateSpeed * horizontalAxis);
            vCam.transform.RotateAround(_centre, Vector3.forward, RotateSpeed * horizontalAxis);
            lastMarbleSpawned.transform.RotateAround(_centre, Vector3.forward, RotateSpeed * horizontalAxis);
        }
        else if (moveRightRequested && !lastMarbleSpawned.GetComponent<PlanetMarble>().isDraggingMouse && !lastMarbleSpawned.GetComponent<PlanetMarble>().marbleMoving)
        {
            moveRightRequested = false;
            playerMarbleSpawner.transform.RotateAround(_centre, Vector3.forward, -RotateSpeed * -horizontalAxis);
            vCam.transform.RotateAround(_centre, Vector3.forward, -RotateSpeed * -horizontalAxis);
            lastMarbleSpawned.transform.RotateAround(_centre, Vector3.forward, -RotateSpeed * -horizontalAxis);
        }
    }

    public void EndPlayerTurn()
    {
        Destroy(lastMarbleSpawned);
        ChangeCurrentPlayer();
        marbleSpawner.SpawnMarble(playerMarbleSpawner.gameObject.transform.position);
    }

    public void ChangeCurrentPlayer()
    {
        currentPlayer = (currentPlayer + 1) % 2;
        if (currentPlayer == 0)
        {
            Player1Turn();
        }
        else if (currentPlayer == 1)
        {
            Player2Turn();
        }
    }

    public void EndPlayerTurnAndCheckWin()
    {
        Destroy(lastMarbleSpawned);
        if (Score[0] == 0 && Score[1] == 0)
        {
            Tie();
        }
        else if (Score[0] == 0)
        {
            Player2Wins();
        }
        else if (Score[1] == 0)
        {
            Player1Wins();
        }
        else
        {
            EndPlayerTurn();
        }
    }

    private void Tie()
    {
        endGameTextUI.GetComponent<TextMeshProUGUI>().text = "Draw";
        endGameTextUI.SetActive(true);
        RestartTheGameAfterSomeTime();
    }

    private void Player1Wins()
    {
        endGameTextUI.GetComponent<TextMeshProUGUI>().text = "Player 1 Wins";
        endGameTextUI.GetComponent<TextMeshProUGUI>().color = Color.red;
        endGameTextUI.SetActive(true);
        RestartTheGameAfterSomeTime();
    }

    private void Player2Wins()
    {
        endGameTextUI.GetComponent<TextMeshProUGUI>().text = "Player 2 Wins";
        endGameTextUI.GetComponent<TextMeshProUGUI>().color = Color.blue;
        endGameTextUI.SetActive(true);
        RestartTheGameAfterSomeTime();
    }

    public void IncreaseScore(int scoreToAdd, int playerIndex)
    {
        Score[playerIndex] += scoreToAdd;
        UpdatePlayerScoresUI(playerIndex);
        Debug.Log("Player " + playerIndex + ": " + Score[playerIndex]);
    }

    private void UpdatePlayerScoresUI(int playerIndex)
    {
        if (playerIndex == 0)
        {
            player1ScoreTextUI.text = Score[0].ToString();
        }
        else if (playerIndex == 1)
        {
            player2ScoreTextUI.text = Score[1].ToString();
        }
    }
}
