﻿using System.Collections.Generic;
using UnityEngine;

public class MarbleSpawner : MonoBehaviour
{
    protected static GameManager gameManager;

    public GameObject marblePrefab;
    private PlanetMarble planetMarbleMovement;

    protected virtual void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
    }

    public void SpawnMarble(Vector3 Position)
    {
        var initializedMarble = Instantiate(marblePrefab, Position, new Quaternion(0, 0, Random.Range(-1.0f, 1.0f), 1));
        initializedMarble.GetComponent<PlanetMarble>().player = gameManager.currentPlayer;
        ApplyPlayerProperties(initializedMarble);
        gameManager.lastMarbleSpawned = initializedMarble;
    }

    protected void ApplyPlayerProperties(GameObject initializedMarble)
    {
        Color colorToApply;
        Sprite spriteToApply;
        switch (gameManager.currentPlayer)
        {
            case 0:
            {
                colorToApply = Color.red;
                spriteToApply = gameManager.playerMarbleSprites[0];
                break;
            }
            case 1:
            {
                colorToApply = Color.blue;
                spriteToApply = gameManager.playerMarbleSprites[1];
                break;
            }
            default:
            {
                colorToApply = new Color(255, 255, 255, 0);
                spriteToApply = gameManager.playerMarbleSprites[0];
                break;
            }
        }
        initializedMarble.GetComponent<PlanetMarble>().ApplyPlayerOutlinerColor(colorToApply);
        initializedMarble.GetComponent<PlanetMarble>().ApplyMarbleSprite(spriteToApply);
    }
}
