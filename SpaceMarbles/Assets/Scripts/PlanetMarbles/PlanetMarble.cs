﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetMarble : MonoBehaviour
{
    private static GameManager gameManager;
    [HideInInspector]
    public SpriteRenderer PlayerOutlinerSpriteRenderer;
    [HideInInspector]
    public SpriteRenderer marbleSpriteRenderer;
    public Sprite marbleSprite;
    public Color playerOutlinerColor;

    [HideInInspector]
    public int player;
    [HideInInspector]
    public int scoreValue = 1;

    [HideInInspector]
    public bool isCurrentMarble = true;

    public float forceMultiplier = 3.0f;
    [SerializeField]
    private Shader lineShader;
    [HideInInspector]
    public bool isDraggingMouse;
    private Vector3 objectInitialPosition;
    private Vector3 clickFinalPosition;

    private Rigidbody2D myRB;
    [HideInInspector]
    public bool marbleMoving;
    private bool justFlicked;
    private bool justReleasedMouse;

    public void ApplyPlayerOutlinerColor(Color playerOutlinerColor)
    {
        this.playerOutlinerColor = playerOutlinerColor;
    }

    public void ApplyMarbleSprite(Sprite marbleSprite)
    {
        this.marbleSprite = marbleSprite;
    }

    private void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        myRB = GetComponent<Rigidbody2D>();
        PlayerOutlinerSpriteRenderer = GetComponentsInChildren<SpriteRenderer>()[1];
        PlayerOutlinerSpriteRenderer.color = playerOutlinerColor;
        marbleSpriteRenderer = GetComponent<SpriteRenderer>();
        marbleSpriteRenderer.sprite = marbleSprite;
    }

    void OnMouseOver()
    {
        if (isCurrentMarble && Input.GetMouseButtonDown(0))
        {
            isDraggingMouse = true;
            objectInitialPosition = gameObject.transform.position;
        }
    }

    private void Update()
    {
        if (isDraggingMouse)
        {
            Vector3 mouseWorldPosition = GetMouseWorldPosition();

            DrawLine(transform.position, mouseWorldPosition, Color.red);
            OnMouseRelease(mouseWorldPosition);
        }
    }

    private void OnMouseRelease(Vector3 mouseWorldPosition)
    {
        if (Input.GetMouseButtonUp(0))
        {
            isCurrentMarble = false;
            isDraggingMouse = false;
            justReleasedMouse = true;
            justFlicked = true;
            clickFinalPosition = mouseWorldPosition;
        }
    }

    private Vector3 GetMouseWorldPosition()
    {
        var mouseWorldPosition = Input.mousePosition;
        mouseWorldPosition.z = 20;
        mouseWorldPosition = Camera.main.ScreenToWorldPoint(mouseWorldPosition);
        return mouseWorldPosition;
    }

    private void FixedUpdate()
    {
        OnMarbleFlicked();
        OnMarbleFlickedStop();
    }

    private void OnMarbleFlicked()
    {
        if (justReleasedMouse)
        {
            var direction = clickFinalPosition - objectInitialPosition;
            justReleasedMouse = false;
            myRB.velocity = -direction * forceMultiplier;
            marbleMoving = true;
        }
    }

    private void OnMarbleFlickedStop()
    {
        if (justFlicked && ((Mathf.Abs(myRB.velocity.x) < 0.1f) && (Mathf.Abs(myRB.velocity.y) < 0.1f)))
        {
            marbleMoving = false;
            justFlicked = false;
            gameManager.EndPlayerTurnAndCheckWin();
        }
    }

    void DrawLine(Vector3 start, Vector3 end, Color color, float duration = 0.02f)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(lineShader);
        lr.startColor = color;
        lr.endColor = color;
        lr.startWidth = 0.1f;
        lr.endWidth = 0.1f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
        GameObject.Destroy(myLine, duration);
    }
}
